import java.nio.file.{Files, Paths}
import java.awt.image.BufferedImage
import java.awt.Color
import java.io._
import scala.io.Source._


//Loads data sets,
//Loads/saves weigths
object Loader {

  var pixels : Array[Byte] = new Array[Byte](1)
  var labels : Array[Byte] = new Array[Byte](1)
  val pixelsOffset = 16
  val labelsOffset = 8

  val weigthsPath = "/home/bartosz/Pulpit/Studia/Scala/DigitNetwork/src/main/resources/weigths.txt"

  def loadTrainSet(): Unit = {
    val pixelsPath = "/home/bartosz/Pulpit/Studia/Scala/DigitNetwork/src/main/resources/train-images.idx3-ubyte"
    val labelsPath = "/home/bartosz/Pulpit/Studia/Scala/DigitNetwork/src/main/resources/train-labels.idx1-ubyte"
    pixels = Files.readAllBytes(Paths.get(pixelsPath))
    labels = Files.readAllBytes(Paths.get(labelsPath))
  }
  def loadTestSet(): Unit = {
    val pixelsPath = "/home/bartosz/Pulpit/Studia/Scala/DigitNetwork/src/main/resources/t10k-images.idx3-ubyte"
    val labelsPath = "/home/bartosz/Pulpit/Studia/Scala/DigitNetwork/src/main/resources/t10k-labels.idx1-ubyte"
    pixels = Files.readAllBytes(Paths.get(pixelsPath))
    labels = Files.readAllBytes(Paths.get(labelsPath))
  }

  var trainNum = 0

  def getNextImage(vec : Array[Double]): Int = {
    val tmpArray : Array[Byte] = new Array[Byte](28*28)
    for (i <- 0 until 28 * 28) {
      tmpArray(i) = (pixels(i + 16 + 28*28*trainNum) & 0xFF).toByte//255
      val x: Int = i % 28
      val y: Int = i / 28
      vec(x+y*29) = tmpArray(i)/255
    }
    val label : Int = labels(8 + trainNum).toInt
    trainNum = trainNum + 1
    trainNum = trainNum % 60000
    label
  }

  def saveWeights(net : NeuralNetwork ): Unit = {
    val pw = new PrintWriter(new File(weigthsPath ))
    pw.write(net.layers.length+"\n")
    for(l <- net.layers) {
      pw.write(l.weigths.length+"\n")
      for(w <- l.weigths) {
        pw.write(w.value +"\n")
      }
    }
    pw.close
  }

  def loadWeights(net : NeuralNetwork): Unit = {
    val lines = fromFile(weigthsPath).getLines
    val layers = lines.next.toInt
    assert(layers == net.layers.length)
    for(l <- 0 until layers) {
      val weigths = lines.next.toInt
      assert(weigths == net.layers(l).weigths.length)
      for (w <- 0 until weigths) {
        net.layers(l).weigths(w).value = lines.next.toDouble
      }
    }
  }
}


//draws images from sets
object Drawer {

  def draw(j : Int, name : String): Unit = {
    val array: Array[Byte] = Loader.pixels
    val dim = 28
    val size = (dim, dim)
    val canvas = new BufferedImage(size._1, size._2, BufferedImage.TYPE_INT_RGB)
    val g = canvas.createGraphics()

    //g.setRenderingHint(java.awt.RenderingHints.KEY_ANTIALIASING,
     // java.awt.RenderingHints.VALUE_ANTIALIAS_ON)

    for (i <- 0 until  dim * dim) {
      val x: Int = i % dim
      val y: Int = i / dim
      ///println(x, y, i)
      val v = 255 - (array(i + 16+ dim*dim*j) & 0xFF)
      //println(v)
      val c = new Color(v, v, v)
      g.setColor(c)
      g.drawLine(x, y, x, y)
    }

    g.dispose()
    javax.imageio.ImageIO.write(canvas, "png", new java.io.File("/home/bartosz/Pulpit/Studia/Scala/DigitNetwork/fails/" +name + ".png"))
  }

}

//builds netwrok with its specific architecture
object Builder {
  val inputSize = 841

  val network = new NeuralNetwork
  // #0
  val layer0 = new Layer(inputSize, 0)
  // #1 - conv, 6 maps 13x13
  val layer1 = new ConvolutionalLayer(6, 13, 5, 1)
  // #2 - conv, 50 maps 5x5
  val layer2 = new ConvolutionalLayer(50, 5, 5, 6)
  // #3 - normal, fully conected
  val layer3 = new Layer(100, 100*(layer2.neurons.length+1))
  // #4 - normal, fully conected
  val layer4 = new Layer(10, 10*(layer3.neurons.length+1))

  network.layers = List(layer0, layer1, layer2, layer3, layer4)

  var maps = layer1.maps
  var mapSize = layer1.mapSize
  var kernelSize = layer1.kernelSize
  var layer : Layer = layer1
  var prevLayer : Layer = layer0
  var kernelTemplate: Array[Int] = Array(
    0,  1,  2,  3,  4,
    29, 30, 31, 32, 33,
    58, 59, 60, 61, 62,
    87, 88, 89, 90, 91,
    116,117,118,119,120)

  for(fmap <- 0 until maps) {
    for(x <- 0 until mapSize) {
      for(y <- 0 until mapSize) {
        var weigths = fmap * 26//(kernelSize * kernelSize + 1)
        var neuron = layer.neurons(y + x * mapSize + fmap*mapSize*mapSize)
        // println(neuron.output)
        var connection = (layer.bias, layer.weigths(weigths))
        neuron.connections += connection
        weigths = weigths + 1

        for(k <- kernelTemplate) {
          connection = (prevLayer.neurons(2*y + 58*x + k), layer.weigths(weigths))
          neuron.connections += connection
          weigths = weigths + 1
        }
      }
    }
  }

  maps = layer2.maps
  mapSize = layer2.mapSize
  kernelSize = layer2.kernelSize
  layer = layer2
  prevLayer = layer1
  kernelTemplate = Array(
    0,  1,  2,  3,  4,
    13, 14, 15, 16, 17,
    26, 27, 28, 29, 30,
    39, 40, 41, 42, 43,
    52, 53, 54, 55, 56)

  for(fmap <- 0 until maps) {
    for(x <- 0 until mapSize) {
      for(y <- 0 until mapSize) {
        var weigths = fmap * 26//(kernelSize * kernelSize + 1)
        var neuron = layer.neurons(y + x * mapSize + fmap*mapSize*mapSize)
        var connection = (layer.bias, layer.weigths(weigths))
        neuron.connections += connection
        weigths = weigths + 1

        for(k <- kernelTemplate) {
          val inputs = List(
            2*y + 26*x + k,
            169 + 2*y + 26*x + k,
            338 + 2*y + 26*x + k,
            507 + 2*y + 26*x + k,
            676 + 2*y + 26*x + k,
            845 + 2*y + 26*x + k)
          for(i <- inputs) {
            // println(i, y, x, k, weigths)
            connection = (prevLayer.neurons(i), layer.weigths(weigths))
            neuron.connections += connection
            weigths = weigths + 1
          }
        }
      }
    }
  }

  var neurons = 100
  var weigths = 0
  layer = layer3
  prevLayer = layer2

  for(n <- 0 until neurons) {
    var neuron = layer.neurons(n)
    var connection = (layer.bias, layer.weigths(weigths))
    neuron.connections += connection
    weigths = weigths + 1
    for(i <- 0 until prevLayer.neurons.length) {
      connection = (prevLayer.neurons(i), layer.weigths(weigths))
      neuron.connections += connection
      weigths = weigths + 1
    }
  }

  neurons = 10
  weigths = 0
  layer = layer4
  prevLayer = layer3

  for(n <- 0 until neurons) {
    var neuron = layer.neurons(n)
    var connection = (layer.bias, layer.weigths(weigths))
    neuron.connections += connection
    weigths = weigths + 1
    for(i <- 0 until prevLayer.neurons.length) {
      connection = (prevLayer.neurons(i), layer.weigths(weigths))
      neuron.connections += connection
      weigths = weigths + 1
    }
  }

}

