class Layer(neuronsN : Int, weigthsN : Int) {

  val neuronsNum : Int = neuronsN
  val weigthsNum : Int = weigthsN

  var nind : Int = -1
  var neurons: Array[Neuron] = Array.fill(neuronsNum){
    nind = nind + 1
    new Neuron(nind)
  }
  var wind : Int = -1
  var weigths: Array[Weigth] = Array.fill(weigthsNum){
    wind = wind + 1
    new Weigth(wind)
  }
  val bias: Neuron = new Neuron(1, -1, true)

  var sigmoid : (Double)  => Double = (x : Double)  => Math.tanh(0.66666667*x) * 1.7159
  var dsigmoid : (Double) => Double = (x : Double) => 0.66666667/1.7159*(1.7159+x)*(1.7159-x)

  //calculates output of neuron
  //from connections with weigths
  def calculate(): Unit = {
    for(neuron <- neurons) {
      var sum : Double = 0

      for(c <- neuron.connections) {
        sum += c._1.output*c._2.value
      }

      neuron.output = sigmoid(sum)
    }
  }


  def backpropagate(input: Array[Double], outputV: Array[Double], eta: Double): Unit = {
    var output = 0.0
    val dErr_wrt_dYn = new Array[Double](neurons.length)
    val dErr_wrt_dWn = new Array[Double](weigths.length)

    for(i <- 0 until neurons.length) {
      output = neurons(i).output
      //println(i, dErr_wrt_dYn.length, input.length)
      dErr_wrt_dYn(i) = dsigmoid(output) * input(i)
    }

    var i = 0
    for(n <- neurons) {
      for((c,w) <- n.connections) {
        output = c.output
        dErr_wrt_dWn(w.index) += dErr_wrt_dYn(i) * output
      }
      i = i+1
    }

    i = 0
    for(n <- neurons) {
      for((c,w) <- n.connections) {
        if(!c.bias) {
          outputV(c.index) += dErr_wrt_dYn(i) * w.value
        }
      }
      i = i+1
    }

    for(w <- weigths) {
      var oldVal = w.value
      var newVal = oldVal - eta * dErr_wrt_dWn(w.index)
      w.value = newVal
    }
  }

}

class ConvolutionalLayer(mapsNum: Int, mapDim: Int, kernelDim : Int, previousMaps : Int) extends Layer(mapsNum*mapDim*mapDim, previousMaps*mapsNum*(kernelDim*kernelDim+1)) {

  val maps: Int = mapsNum
  val mapSize : Int = mapDim
  val kernelSize : Int = kernelDim
}