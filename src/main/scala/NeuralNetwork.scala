import java.util.Calendar

class NeuralNetwork {
  var layers : List[Layer] = List()
  var eta = 0.0065

  //recognize single image
  def recognize(): (Int, Int) = {
    var input: Array[Double] = Array.fill[Double](841)(0)
    var output: Array[Double] = Array.fill[Double](10)(0)

    val label = Loader.getNextImage(input)
    propagate(input, output)
    val result = output.zipWithIndex.maxBy(_._1)._2
    (result, label)
  }

  //one epoch consists of:
  //propagation
  //backpropagation
  def epoch(): Unit = {
    var successes = 0
    for(i <- 0 until 60000) {
      //if(i %1000 == 0) {
       // val now = Calendar.getInstance()
      //  println(i/1000, now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE))
      //}
      var input: Array[Double] = new Array[Double](841)
      var output: Array[Double] = new Array[Double](10)
      var desired: Array[Double] = new Array[Double](10)

      input = Array.fill[Double](841)(0)
      var label = Loader.getNextImage(input)
      propagate(input, output)
      val res = output.zipWithIndex.maxBy(_._1)._2
      desired(label) = 1
      backpropagate(output, desired)
      desired(label) = 0
      if(res == label)
        successes += 1
    }
    println("Succ rate: " + (successes.toDouble/60000))
  }

  //propagates signal trhough network
  def propagate(inputVector: Array[Double], outputVector: Array[Double]): Unit = {
    //first layer is simple the same as input
    for((input, n) <- inputVector zip layers.head.neurons ) {
      n.output = input
    }

    for(l <- layers.slice(1, layers.length)) {
      l.calculate()
    }

    for(i <- 0 until layers.last.neurons.length) {
//      println(layers.last.neurons.length)
      outputVector(i) = layers.last.neurons(i).output
    }
  }

  //propagate signal back correcting error
  def backpropagate(outputVector: Array[Double], desiredVector: Array[Double]): Unit = {

    val size = layers.length
    var differentials = new Array[Array[Double]](size)

    val errorOnLast =  for((o, d) <- outputVector zip desiredVector) yield o-d

    differentials(size-1) = errorOnLast.toArray
    for(i <- 0 until size-1) {
      differentials(i) = new Array[Double](layers(i).neurons.length)
      //println(differentials(i).length)
    }

    var i = size-1
    for(l <- layers.drop(1).reverse) {
      l.backpropagate(differentials(i), differentials(i-1), eta)
      i = i-1
    }
  }

}
