import scala.collection.mutable.ListBuffer

class Neuron(ind : Int) {
  var output : Double = 0
  var connections : ListBuffer[(Neuron, Weigth)] = ListBuffer()
  var bias = false
  val index = ind

  def this(init: Double, ind : Int) {
    this(ind)
    output = init
  }
  def this(init: Double, ind : Int, isBias: Boolean) {
    this(ind)
    output = init
    bias = isBias
  }
}

class Weigth(initValue : Double, ind : Int) {

  var value: Double = initValue
  val index: Int = ind

  def this(ind : Int) {
    this(0.05*(math.random*2 - 1), ind)
  }
}

