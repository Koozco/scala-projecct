object Tester extends App{
  val network = Builder.network
  Loader.loadTestSet()
  val pixels = Loader.pixels
  val labels = Loader.labels

  Loader.loadWeights(network)
  val testsNum = 10000
  var fails = 0
  var successes = 0
  val counters : Array[Int] = new Array[Int](10)

  for(test <- 0 until testsNum) {
    val (result : Int, desired : Int) = network.recognize()
    if(result == desired)
      successes += 1
    else {
      fails += + 1
      counters(desired) += 1
      val name : String = "expected"+desired+"_was"+result
      Drawer.draw(test, name)
    }
    if( test % 500 == 0)
      println((test+1) + " finished.")
  }

  val succesRate : Double = successes.toDouble / testsNum
  println("Succes rate: " + succesRate, successes)
  println("Fails: " + fails)
//  for(c <- counters.zipWithIndex) {
//    println("\t" + c._2 +": "+c._1+" ("+( c._1.toDouble / testsNum : Double)+")")
//  }
}
