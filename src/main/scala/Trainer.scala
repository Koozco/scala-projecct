object Trainer extends App {
  val network = Builder.network
  Loader.loadTrainSet()
  val pixels = Loader.pixels
  val labels = Loader.labels

  for(ii <- 0 until 100) {
    Loader.loadWeights(network)
    network.epoch()
    Loader.saveWeights(network)
    //println((ii + 1) + " finished.")
  }
}
